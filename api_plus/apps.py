from django.apps import AppConfig


class ApiPlusConfig(AppConfig):
    name = 'api_plus'
