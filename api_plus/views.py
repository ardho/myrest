from django.http import JsonResponse
import json

# Create your views here.
def index(request):
	if request.method == 'GET':
		ans = str(int(request.GET['num1']) + int(request.GET['num2']))
		return JsonResponse({'ans':ans})
